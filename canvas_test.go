package canvas

import (
	"image"
	"image/color"
	"image/png"
	"math"
	"os"
	"testing"
)

func TestCreate(t *testing.T) {
	cnv := New(4, 4)
	filename := "testImages/create.png"
	testImages(t, filename, cnv)
}

func TestCreateRectSimple(t *testing.T) {
	cnv := New(4, 4)
	ctx := cnv.Get2DContext()
	ctx.Rect(1, 1, 2, 2)
	ctx.StrokeStyle = Black
	ctx.Stroke()

	filename := "testImages/create_rect_simple.png"
	testImages(t, filename, cnv)
}

func TestSmallSimpleFill(t *testing.T) {
	cnv := New(4, 4)
	ctx := cnv.Get2DContext()

	ctx.Rect(0, 0, 4, 4)
	ctx.FillStyle = Black
	ctx.Fill()

	filename := "testImages/4x4_rect_fill.png"
	testImages(t, filename, cnv)

}

func TestSmallSimpleFill2(t *testing.T) {
	cnv := New(4, 4)
	ctx := cnv.Get2DContext()

	ctx.Rect(1, 1, 2, 2)
	ctx.FillStyle = Black
	ctx.Fill()

	filename := "testImages/4x4_rect_fill2.png"
	testImages(t, filename, cnv)
}

func TestSmallSimpleFill3(t *testing.T) {
	cnv := New(5, 5)
	ctx := cnv.Get2DContext()

	ctx.Rect(1, 1, 3, 3)
	ctx.FillStyle = Black
	ctx.Fill()

	filename := "testImages/5x5_3x3_rect_fill.png"
	testImages(t, filename, cnv)
}

func TestGradientRects(t *testing.T) {
	cnv := New(150, 150)
	ctx := cnv.Get2DContext()
	for i := 0; i < 6; i += 1 {
		for j := 0; j < 6; j += 1 {
			r := uint8(math.Floor(255.0 - 42.5*float64(i)))
			g := uint8(math.Floor(255.0 - 42.5*float64(j)))
			var b uint8 = 0
			ctx.FillStyle = color.RGBA{
				r, g, b, 0xff,
			}
			ctx.FillRect(j*25, i*25, 25, 25)
		}

	}
	filename := "testImages/150x150_gradient_rects.png"
	testImages(t, filename, cnv)
}

func TestMultiFill(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()

	ctx.BeginPath()
	ctx.Rect(0, 0, 8, 8)
	ctx.FillStyle = Black
	ctx.Fill()

	ctx.BeginPath()
	ctx.Rect(1, 1, 6, 6)
	ctx.FillStyle = Red
	ctx.Fill()

	ctx.BeginPath()
	ctx.Rect(2, 2, 4, 4)
	ctx.FillStyle = Yellow
	ctx.Fill()

	ctx.BeginPath()
	ctx.Rect(3, 3, 2, 2)
	ctx.FillStyle = Purple
	ctx.Fill()

	filename := "testImages/8x8_multi_rect_fill.png"
	testImages(t, filename, cnv)
}

func TestTriangleFill1(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Purple

	ctx.BeginPath()
	ctx.MoveTo(4, 1)
	ctx.LineTo(7, 4)
	ctx.LineTo(1, 4)
	ctx.ClosePath()
	ctx.Fill()

	filename := "testImages/8x8_tri_1.png"
	testImages(t, filename, cnv)
}

func TestTriangleFill2(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Purple

	ctx.BeginPath()
	ctx.MoveTo(1, 3)
	ctx.LineTo(4, 6)
	ctx.LineTo(7, 3)
	ctx.ClosePath()
	ctx.Fill()

	filename := "testImages/8x8_tri_2.png"
	testImages(t, filename, cnv)
}

func TestTriangleFill3(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Purple

	ctx.BeginPath()
	ctx.MoveTo(4, 1)
	ctx.LineTo(1, 4)
	ctx.LineTo(4, 7)
	ctx.ClosePath()
	ctx.Fill()

	filename := "testImages/8x8_tri_3.png"
	testImages(t, filename, cnv)
}

func TestTriangleFill4(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Purple

	ctx.BeginPath()
	ctx.MoveTo(6, 3)
	ctx.LineTo(3, 6)
	ctx.LineTo(3, 0)
	ctx.ClosePath()
	ctx.Fill()

	filename := "testImages/8x8_tri_4.png"
	testImages(t, filename, cnv)
}

func TestDualTriangleFill(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Red

	ctx.BeginPath()
	ctx.MoveTo(0, 1)
	ctx.LineTo(6, 7)
	ctx.LineTo(0, 7)
	ctx.ClosePath()
	ctx.Fill()

	ctx.FillStyle = Green
	ctx.BeginPath()
	ctx.MoveTo(1, 0)
	ctx.LineTo(7, 0)
	ctx.LineTo(7, 6)
	ctx.ClosePath()
	ctx.Fill()

	filename := "testImages/8x8_dual_tri.png"
	testImages(t, filename, cnv)
}

func TestDualTriangleAndRectFill(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.FillStyle = Red

	ctx.BeginPath()
	ctx.MoveTo(0, 1)
	ctx.LineTo(6, 7)
	ctx.LineTo(0, 7)
	ctx.ClosePath()
	ctx.Fill()

	ctx.FillStyle = Green
	ctx.BeginPath()
	ctx.MoveTo(1, 0)
	ctx.LineTo(7, 0)
	ctx.LineTo(7, 6)
	ctx.ClosePath()
	ctx.Rect(1, 5, 2, 2)
	ctx.Fill()

	ctx.FillStyle = Red
	ctx.BeginPath()
	ctx.Rect(5, 1, 2, 2)
	ctx.Fill()

	filename := "testImages/8x8_dual_tri_rects.png"
	testImages(t, filename, cnv)
}

func TestOffsetTriangle1(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.BeginPath()
	ctx.MoveTo(0, 0)
	ctx.LineTo(7, 1)
	ctx.LineTo(1, 7)
	ctx.ClosePath()
	ctx.FillStyle = Yellow
	ctx.Fill()
	filename := "testImages/8x8_offset_tri_1.png"
	testImages(t, filename, cnv)
}

func TestOffsetTriangle2(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.BeginPath()
	ctx.MoveTo(0, 0)
	ctx.LineTo(7, 0)
	ctx.LineTo(1, 7)
	ctx.ClosePath()
	ctx.FillStyle = Yellow
	ctx.Fill()
	filename := "testImages/8x8_offset_tri_2.png"
	testImages(t, filename, cnv)
}

func TestSimplePolyFill(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.BeginPath()
	ctx.MoveTo(0, 7)
	ctx.LineTo(0, 3)
	ctx.LineTo(2, 3)
	ctx.LineTo(2, 1)
	ctx.LineTo(5, 1)
	ctx.LineTo(5, 3)
	ctx.LineTo(7, 3)
	ctx.LineTo(7, 7)
	ctx.ClosePath()
	ctx.FillStyle = Yellow
	ctx.Fill()

	filename := "testImages/8x8_simple_poly_fill.png"
	testImages(t, filename, cnv)
}

func TestCreate8x8WithBlackRect(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	ctx.Rect(2, 2, 4, 4)
	ctx.StrokeStyle = Black
	ctx.Stroke()

	filename := "testImages/8x8_4x4_rect.png"
	testImages(t, filename, cnv)
}

func TestCreate8x8WithDoubleRect(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	// black outer
	ctx.BeginPath()
	ctx.Rect(2, 2, 4, 4)
	ctx.StrokeStyle = Black
	ctx.Stroke()
	// white inner
	ctx.BeginPath()
	ctx.Rect(3, 3, 2, 2)
	ctx.StrokeStyle = White
	ctx.Stroke()

	filename := "testImages/8x8_double_rect.png"
	testImages(t, filename, cnv)
}

func Test8x8RectOverlayOnRect(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()
	// black outer
	ctx.BeginPath()
	ctx.Rect(2, 2, 4, 4)
	ctx.StrokeStyle = Black
	ctx.Stroke()
	// white inner
	ctx.BeginPath()
	ctx.Rect(3, 3, 2, 2)
	ctx.StrokeStyle = White
	ctx.Stroke()
	//blue top
	ctx.BeginPath()
	ctx.Rect(2, 0, 4, 3)
	ctx.StrokeStyle = Blue
	ctx.Stroke()

	filename := "testImages/8x8_rect_overlay.png"
	testImages(t, filename, cnv)
}

func TestBresenham(t *testing.T) {
	start, stop := image.Point{0, 7}, image.Point{7, 0}
	expectedPts := []image.Point{
		image.Point{7, 0},
		image.Point{6, 1},
		image.Point{5, 2},
		image.Point{4, 3},
		image.Point{3, 4},
		image.Point{2, 5},
		image.Point{1, 6},
		image.Point{0, 7},
	}
	actualPts := bresenhams(start, stop)
	if len(actualPts) != len(expectedPts) {
		t.Fatalf("Wrong number of points. Expected %d, but got %d", len(expectedPts), len(actualPts))
	}
	for idx := range actualPts {
		if !pointEquals(actualPts[idx], expectedPts[idx]) {
			t.Fatalf("Wrong point. Expected %v, got %v", expectedPts[idx], actualPts[idx])
		}
	}
}

func Test8x8Lines(t *testing.T) {
	cnv := New(8, 8)
	ctx := cnv.Get2DContext()

	ctx.BeginPath()
	ctx.StrokeStyle = Black
	ctx.MoveTo(0, 7)
	ctx.LineTo(7, 0)
	ctx.Stroke()
	ctx.ClosePath()

	ctx.BeginPath()
	//red line
	ctx.StrokeStyle = Red
	ctx.MoveTo(0, 0)
	ctx.LineTo(7, 1)
	ctx.Stroke()
	ctx.ClosePath()

	ctx.BeginPath()
	// yellow
	ctx.StrokeStyle = Yellow
	ctx.MoveTo(0, 0)
	ctx.LineTo(1, 7)
	ctx.Stroke()

	filename := "testImages/8x8_lines.png"
	testImages(t, filename, cnv)
}

// Utility below

func colorEquals(A, B color.Color) bool {
	AR, AG, AB, AA := A.RGBA()
	BR, BG, BB, BA := B.RGBA()
	return AR == BR && AG == BG && AB == BB && AA == BA
}

func testImages(t *testing.T, loc string, cnv *Canvas) {
	file, err := os.Open(loc)
	if err != nil {
		t.Error(err)
	}
	expectedImg, err := png.Decode(file)
	if err != nil {
		t.Error(err)
	}
	actualImg := cnv.img
	actualBounds, expectedBounds := actualImg.Bounds(), expectedImg.Bounds()
	if actualBounds.Dx() != expectedBounds.Dx() || actualBounds.Dy() != expectedBounds.Dy() {
		t.Fatalf("Expected dims: [%d,%d]. Actual: [%d, %d]",
			expectedBounds.Dx(),
			expectedBounds.Dy(),
			actualBounds.Dx(),
			actualBounds.Dy(),
		)
	}
	for y := 0; y < actualBounds.Dy(); y += 1 {
		for x := 0; x < actualBounds.Dx(); x += 1 {
			actualColor := actualImg.At(x, y)
			expectedColor := expectedImg.At(x, y)
			if !colorEquals(actualColor, expectedColor) {
				t.Errorf("Color mismatch at [%d,%d]. Expected %v, but got %v", x, y, expectedColor, actualColor)
			}
		}
	}
}
