package canvas

import (
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"io"
	"math"
)

// Color constants
var (
	Black  = color.Gray16{0}
	White  = color.Gray16{0xffff}
	Red    = color.RGBA{0xff, 0, 0, 0xff}
	Green  = color.RGBA{0, 0xff, 0, 0xff}
	Blue   = color.RGBA{0, 0, 0xff, 0xff}
	Yellow = color.RGBA{0xff, 0xff, 0, 0xff}
	Purple = color.RGBA{0xff, 0, 0xff, 0xff}
)

// Canvas represents a javascript canvas object
//
// To start drawing calling the Get2DContext method
type Canvas struct {
	img *image.RGBA
}

type Context2D struct {
	canvas   *Canvas
	lastPath *path
	// FillStyle can be set to control the fill color of the fill related funcs
	FillStyle color.Color
	// StrokeStyle can be set to control the stroke color of the stroke related funcs
	StrokeStyle color.Color
}

type path struct {
	subpaths        []subpath
	needsNewSubpath bool
}
type subpath struct {
	closed bool
	points []image.Point
	joins  []joinable
}

type joinable interface {
	getPoints(start, stop image.Point) []image.Point
	crossesPath(start, stop, poi image.Point) bool
}

// New creates a new canvas with specified size.
//
// Do not just create a canvas object or stuff will be funky
func New(width, height int) *Canvas {
	return &Canvas{
		img: image.NewRGBA(
			image.Rectangle{
				image.Point{0, 0},
				image.Point{width, height},
			},
		),
	}
}

// Save will encode the canvas as the given format in string and output to writer.
//
// Only supported formats currently are jpg and png
// jpg will save with the default golang quality
func (c *Canvas) Save(writer io.Writer, format string) {
	switch format {
	case "png":
		png.Encode(writer, c.img)
	case "jpeg":
		fallthrough
	case "jpg":
		jpeg.Encode(writer, c.img, &jpeg.Options{jpeg.DefaultQuality})
	}
}

func (c *Canvas) Get2DContext() *Context2D {
	return &Context2D{
		canvas:      c,
		lastPath:    &path{},
		FillStyle:   Black,
		StrokeStyle: White,
	}
}

func (p *path) addPointToLastSubpath(x, y int) {
	lastIdx := len(p.subpaths) - 1
	p.subpaths[lastIdx].points = append(p.subpaths[lastIdx].points, image.Point{x, y})
}
func (p *path) addJoinToLastSubpath(j joinable) {
	lastIdx := len(p.subpaths) - 1
	p.subpaths[lastIdx].joins = append(p.subpaths[lastIdx].joins, j)
}

func (ctx *Context2D) ensureSubPath(x, y int) {
	if ctx.lastPath.needsNewSubpath {
		ctx.MoveTo(x, y)
		ctx.lastPath.needsNewSubpath = false
	}
}

// MoveTo begins a new sub-path at the point specified by the given (x, y) coordinates.
//
// To draw the path onto a canvas, you can use the Fill() or Stroke() methods.
func (ctx *Context2D) MoveTo(x, y int) {
	// https://html.spec.whatwg.org/multipage/canvas.html#dom-context-2d-moveto
	ctx.lastPath.subpaths = append(ctx.lastPath.subpaths, subpath{
		closed: false,
		points: []image.Point{image.Point{x, y}},
	})

}

// LineTo adds a straight line to the current sub-path by connecting the sub-path's last point to the specified (x, y) coordinates.
func (ctx *Context2D) LineTo(x, y int) {
	if len(ctx.lastPath.subpaths) == 0 {
		ctx.ensureSubPath(x, y)
	} else {
		ctx.lastPath.addPointToLastSubpath(x, y)
		ctx.lastPath.addJoinToLastSubpath(lineJoin{})
	}

}

// BeginPath starts a new path by emptying the list of sub-paths.
//
// Call this method when you want to create a new path.
func (ctx *Context2D) BeginPath() {
	ctx.lastPath = &path{
		needsNewSubpath: true,
		subpaths:        []subpath{},
	}
}

// ClosePath attempts to add a straight line from the current point to the start of the current sub-path.
//
// If the shape has already been closed or has only one point, this function does nothing.
func (ctx *Context2D) ClosePath() {
	// https://html.spec.whatwg.org/multipage/canvas.html#dom-context-2d-closepath
	if len(ctx.lastPath.subpaths) == 0 {
		return
	}
	lastIdx := len(ctx.lastPath.subpaths) - 1
	ctx.lastPath.subpaths[lastIdx].closed = true
	firstPoint := ctx.lastPath.subpaths[lastIdx].points[0] // get first point of last subpath
	ctx.lastPath.subpaths = append(ctx.lastPath.subpaths, subpath{
		closed: false,
		points: []image.Point{firstPoint},
	})
}

func getRectSubpaths(x, y, w, h int) []subpath {
	pts := []image.Point{
		image.Point{x, y},
		image.Point{x + w - 1, y},
		image.Point{x + w - 1, y + h - 1},
		image.Point{x, y + h - 1},
	}
	return []subpath{
		subpath{
			closed: true,
			points: pts,
			joins: []joinable{
				lineJoin{},
				lineJoin{},
				lineJoin{},
			},
		},
		subpath{
			points: []image.Point{image.Point{x, y}},
		},
	}
}

// FillRect draws a rectangle that is filled according to the current fillStyle.
//
// This method draws directly to the canvas without modifying the current path, so any subsequent fill() or stroke() calls will have no effect on it.
func (ctx *Context2D) FillRect(x, y, w, h int) {
	rects := getRectSubpaths(x, y, w, h)
	ctx.fillPath(rects)
}

// Rect adds a rectangle to the current subpath.
func (ctx *Context2D) Rect(x, y, w, h int) {
	rects := getRectSubpaths(x, y, w, h)
	ctx.lastPath.subpaths = append(ctx.lastPath.subpaths, rects...)
}

// Fill fills the current path with the "evenodd" odd fill rule.
func (ctx *Context2D) Fill() {
	ctx.fillPath(ctx.lastPath.subpaths)
}
func (ctx *Context2D) fillPath(subpaths []subpath) {

	for _, path := range subpaths {
		if len(path.points) == 0 {
			continue
		}
		// this isn't what I want to do here, but I'll leave it like this for now
		if len(path.points) == 1 {
			continue
		}
		rect := getEnclosingRectangle(path)
		/*
			instead of iterating over every point in the Canvas
			we'll just get the rect that encloses the path and iterate over those
			hopefully it'll be smaller and go faster since I can't think of another soln.
		*/
		min, max := rect.Min, rect.Max
		for y := min.Y; y < max.Y+1; y += 1 {
			for x := min.X; x < max.X+1; x += 1 {
				c := false
				for idx, join := range path.joins {
					pt1, pt2 := path.points[idx], path.points[idx+1]
					if join.crossesPath(pt1, pt2, image.Point{x, y}) {
						c = !c
					}
				}
				if path.closed { // if it's closed, connect first and last with line
					start, stop := path.points[len(path.points)-1], path.points[0]
					lj := lineJoin{}
					if lj.crossesPath(start, stop, image.Point{x, y}) {
						c = !c
					}
				}
				if c {
					ctx.canvas.img.Set(x, y, ctx.FillStyle)
				}
			}
		}
	}
	prevStrokeColor := ctx.StrokeStyle
	ctx.StrokeStyle = ctx.FillStyle
	ctx.strokeSubpath(subpaths)
	ctx.StrokeStyle = prevStrokeColor
}

func (ctx *Context2D) strokeSubpath(subpaths []subpath) {
	for _, path := range subpaths {
		if len(path.points) == 0 {
			continue
		}
		// this isn't what I want to do here, but I'll leave it like this for now
		if len(path.points) == 1 {
			continue
		}

		for joinIdx, join := range path.joins {
			start, stop := path.points[joinIdx], path.points[joinIdx+1]
			pts := join.getPoints(start, stop)
			for _, pixel := range pts {
				ctx.canvas.img.Set(pixel.X, pixel.Y, ctx.StrokeStyle)
			}
		}
		if path.closed { // if it's closed, connect first and last with line
			start, stop := path.points[len(path.points)-1], path.points[0]
			pts := lineJoin{}.getPoints(start, stop)
			for _, pixel := range pts {
				ctx.canvas.img.Set(pixel.X, pixel.Y, ctx.StrokeStyle)
			}
		}

	}
}

// Stroke method  strokes (outlines) the current path with the current StrokeStyle color
func (ctx *Context2D) Stroke() {
	ctx.strokeSubpath(ctx.lastPath.subpaths)
}

/* Utility Funcs */

func pair(pts []image.Point) [][]image.Point {
	pairs := make([][]image.Point, 0, len(pts))
	for idx := 1; idx < len(pts); idx += 1 {
		pairs = append(pairs, []image.Point{
			pts[idx-1],
			pts[idx],
		})
	}
	return pairs
}

type lineJoin struct {
}

func (lj lineJoin) getPoints(start, end image.Point) []image.Point {
	var pts []image.Point
	if start.X-end.X == 0 && start.Y-end.Y != 0 { // vert line
		changing := intRange(start.Y, end.Y)
		pts = make([]image.Point, len(changing))
		for idx := range changing {
			pts[idx] = image.Point{start.X, changing[idx]}
		}

	} else if start.Y-end.Y == 0 && start.X-end.X != 0 { //horiz line
		changing := intRange(start.X, end.X)
		pts = make([]image.Point, len(changing))
		for idx := range changing {
			pts[idx] = image.Point{changing[idx], start.Y}
		}
	} else {
		pts = bresenhams(start, end)
	}
	return pts

}

func (lj lineJoin) crossesPath(start, stop, poi image.Point) bool {

	if start.Y < poi.Y && stop.Y < poi.Y {
		return false
	}
	if start.Y >= poi.Y && stop.Y >= poi.Y {
		return false
	}
	// X, Y should be int so only float the result before div
	revSlope := (float64(stop.X - start.X)) / float64(stop.Y-start.Y)
	xIntersect := float64(poi.Y-start.Y)*revSlope + float64(start.X)
	return xIntersect >= float64(poi.X)

}

func intRange(start, end int) []int {
	if start > end {
		end, start = start, end
	}
	pts := make([]int, end-start+1)
	for idx := range pts {
		pts[idx] = start + idx
	}
	return pts
}

func bresenhams(start, end image.Point) []image.Point {
	x0, y0 := start.X, start.Y
	x1, y1 := end.X, end.Y
	if abs(y1-y0) < abs(x1-x0) {
		if x0 > x1 {
			return bresenhamsLineLow(x1, y1, x0, y0)
		} else {
			return bresenhamsLineLow(x0, y0, x1, y1)
		}
	} else {
		if y0 > y1 {
			return bresenhamsLineHigh(x1, y1, x0, y0)
		} else {
			return bresenhamsLineHigh(x0, y0, x1, y1)
		}
	}
}

func abs(b int) int {
	if b < 0 {
		return -b
	}
	return b
}

func bresenhamsLineLow(x0, y0, x1, y1 int) []image.Point {
	pts := make([]image.Point, 0, 16)
	dx, dy := x1-x0, y1-y0
	yi := 1
	if dy < 0 {
		yi = -1
		dy = -dy
	}
	D := 2*dy - dx
	y := y0
	for x := x0; x < x1+1; x += 1 {
		pts = append(pts, image.Point{x, y})
		if D > 0 {
			y += yi
			D -= 2 * dx
		}
		D += 2 * dy
	}
	return pts
}
func bresenhamsLineHigh(x0, y0, x1, y1 int) []image.Point {
	pts := make([]image.Point, 0, 16)
	dx, dy := x1-x0, y1-y0
	xi := 1
	if dx < 0 {
		xi = -1
		dx = -dx
	}
	D := 2*dx - dy
	x := x0
	for y := y0; y < y1+1; y += 1 {
		pts = append(pts, image.Point{x, y})
		if D > 0 {
			x += xi
			D -= 2 * dy
		}
		D += 2 * dx
	}
	return pts
}

func getEnclosingRectangle(path subpath) image.Rectangle {
	minX, minY := math.MaxInt32, math.MaxInt32
	maxX, maxY := math.MinInt32, math.MinInt32
	for idx, join := range path.joins {
		switch join.(type) {
		case lineJoin:
			pt1, pt2 := path.points[idx], path.points[idx+1]
			minX, maxX = min(minX, pt1.X, pt2.X), max(maxX, pt1.X, pt2.X)
			minY, maxY = min(minY, pt1.Y, pt2.Y), max(maxY, pt1.Y, pt2.Y)
		}
	}
	return image.Rect(minX, minY, maxX, maxY)
}
func max(l ...int) int {
	if len(l) == 0 {
		return math.MinInt32
	}
	if len(l) == 1 {
		return l[0]
	}
	mx := l[0]
	for i := 1; i < len(l); i += 1 {
		if l[i] > mx {
			mx = l[i]
		}
	}
	return mx
}
func min(l ...int) int {
	if len(l) == 0 {
		return math.MaxInt32
	}
	if len(l) == 1 {
		return l[0]
	}
	mn := l[0]
	for i := 1; i < len(l); i += 1 {
		if l[i] < mn {
			mn = l[i]
		}
	}
	return mn
}

func pointEquals(A, B image.Point) bool {
	x0, y0 := A.X, A.Y
	x1, y1 := B.X, B.Y
	return x0 == x1 && y0 == y1
}

func floatEquals(A, B, tol float64) bool {
	return math.Abs(A-B) <= tol
}
